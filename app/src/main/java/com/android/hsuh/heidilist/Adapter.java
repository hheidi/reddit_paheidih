package com.android.hsuh.heidilist;


import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import android.net.Uri;


public class Adapter extends RecyclerView.Adapter<PostHolder> {

    private ArrayList<ToDoItem> ToDoItem;
    private ActivityCallBack activityCallBack;

    public Adapter(MainActivity activityCallBack, ArrayList<ToDoItem> ToDoItem) {
        this.activityCallBack = activityCallBack;
        this.ToDoItem = ToDoItem;
    }


    @Override
    public PostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return  new  PostHolder(view);
    }

    @Override
    public void onBindViewHolder(PostHolder holder, final int position) {
        holder.titleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityCallBack.onPostSelected(position);}
        });

        holder.titleText.setText(ToDoItem.get(position).title);
    }

    @Override
    public int getItemCount() {
        return ToDoItem.size();
    }
}
