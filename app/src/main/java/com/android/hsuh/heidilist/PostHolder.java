package com.android.hsuh.heidilist;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class PostHolder extends RecyclerView.ViewHolder {
    public TextView titleText;

    public PostHolder(View itemView) {
        super(itemView);
        titleText = (TextView) itemView;
    }
}
