package com.android.hsuh.heidilist;

import android.app.FragmentTransaction;
import android.app.Fragment;
import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends SingleFragmentActivity implements ActivityCallBack {

    public ArrayList<ToDoItem> list = new ArrayList<ToDoItem>();

    @Override
    protected Fragment createFragment() {
        return new ToDoFragment();
    }


    @Override
    protected int getLayoutResId() {
        return R.layout.activity_fragment;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPostSelected(int pos) {
       // currentItem = pos;
        Fragment newFragment = new ItemFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}