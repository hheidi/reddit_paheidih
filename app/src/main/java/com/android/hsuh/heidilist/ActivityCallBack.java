package com.android.hsuh.heidilist;
import android.net.Uri;

public interface ActivityCallBack {
    void onPostSelected(int position);
}
