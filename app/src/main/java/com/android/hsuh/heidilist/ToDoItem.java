package com.android.hsuh.heidilist;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;

public class ToDoItem {

    public String title;
    public String dateAdded;
    public String dueDate;
    public String category;

    public ToDoItem (String title, String dateAdded, String dueDate, String category) {
        this.title = title;
        this. dueDate =  dueDate;
        this. dateAdded = dateAdded;
        this. category = category;


    }
};

