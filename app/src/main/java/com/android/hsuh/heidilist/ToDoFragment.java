

package com.android.hsuh.heidilist;

        import android.app.Activity;
        import android.content.Context;
        import android.os.Bundle;
        import android.support.annotation.Nullable;
        import android.app.Fragment;
        import android.support.v7.widget.LinearLayoutManager;
        import android.support.v7.widget.RecyclerView;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.webkit.WebView;

public class ToDoFragment extends Fragment {

    private MainActivity activity;
    private RecyclerView recyclerView;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (MainActivity)activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_layout, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        ToDoItem ti1 = new ToDoItem("hi", "yes", "it's", "me");
        ToDoItem ti2 = new ToDoItem("hello", "yes", "it's", "me");
        ToDoItem ti3 = new ToDoItem("yes", "yes", "it's", "me");
        ToDoItem ti4 = new ToDoItem("ok", "yes", "it's", "me");

        activity.list.add(ti1);
        activity.list.add(ti2);
        activity.list.add(ti3);
        activity.list.add(ti4);

        Adapter adapter = new Adapter(activity, activity.list );
        recyclerView.setAdapter(adapter);

        return view;
    }
}